#make dtbs

cat arch/arm64/boot/Image.gz arch/arm64/boot/dts/qcom/sm8250-instantnoodlep.dtb > /tmp/kernel-dtb

mkbootimg --base 0x0 --cmdline PMOS_NO_OUTPUT_REDIRECT --kernel_offset 0x8000 --ramdisk_offset 0x1000000 --tags_offset 0x100 --pagesize 4096 --second_offset 0xf00000 --ramdisk /home/jay/instantnoodlep-mainline/initramfs-ip --kernel /tmp/kernel-dtb -o /home/jay/instantnoodlep-mainline/output/mainline-boot.img
